//
//  Localization.swift
//  Wahda Bank
//
//  Created by Apple on 27/01/22.
//

import UIKit

class  ManageLocalization {
    static var deviceLang = ""
    class func getdeviceLangBundle()-> Bundle{
        // To get Language of device ...
        var path : String = String()

        if deviceLang.isEmpty {
            if let preferredLanguage = NSLocale.preferredLanguages[0] as String? {
                 let language = (preferredLanguage as NSString).substring(to: 2)
                deviceLang = language
            }
        }
            path = Bundle.main.path(forResource: deviceLang, ofType: "lproj")!
        return Bundle(path: path)!
    }
    
    class func getLocalaizedString(key:String)-> String{
        return NSLocalizedString(key, tableName: nil, bundle: getdeviceLangBundle(), value: "", comment: "")
    }
}

