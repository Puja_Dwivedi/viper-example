//
//  DrawerRootViewController.swift
//  SlideMenu
//
//  Created by cis on 17/08/17.
//  Copyright © 2017 cis. All rights reserved.
//

import UIKit
import REFrostedViewController

class DrawerRootViewController: REFrostedViewController {
    override func awakeFromNib() {
        self.contentViewController = self.storyboard?.instantiateViewController(withIdentifier: "DrawerRootNavigationController")
        self.menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController")
        self.menuViewSize.width = UIScreen.main.bounds.size.width/2
        if ManageLocalization.deviceLang == "ar" {
            self.direction = .right
        }else{
            self.direction = .left
        }
        
    }
}
