//
//  ViewControllerTest.swift
//  Add-Code
//
//  Created by cis on 20/12/17.
//  Copyright © 2017 CIS. All rights reserved.
//

import UIKit

class ViewControllerTest: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btn_Open(_ sender: UIButton) {
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
        
    }

}
