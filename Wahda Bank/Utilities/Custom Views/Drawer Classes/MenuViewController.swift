//
//  MenuViewController.swift
//  SlideMenu
//
//  Created by cis on 17/08/17.
//  Copyright © 2017 cis. All rights reserved.
//

import UIKit
import REFrostedViewController
import SDWebImage

class cellMenu : UITableViewCell{
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var imgIcon : UIImageView!
}

class MenuViewController: UIViewController, REFrostedViewControllerDelegate {
    
    var selectedCell = 0
    
    @IBOutlet var imgUserImage : UIImageView!
    @IBOutlet var tblMenu : UITableView!
    @IBOutlet var lblName : UILabel!
    
    let dict = NSDictionary(object: "Home", forKey: "icon" as NSCopying)
    
    //var arrMenuList = NSMutableArray(array: ["Home","Send Private","Receive Private","Transaction","My Account","Signout"])
    
    var arrMenuList = NSMutableArray(array: [["icon":"img_Inbox_icon","title":ManageLocalization.getLocalaizedString(key: "Inbox")],
                                             ["icon":"img_compose_icon","title":ManageLocalization.getLocalaizedString(key: "Compose")],
                                             ["icon":"img_sent_email_icon","title":ManageLocalization.getLocalaizedString(key: "Sent Mail")],
                                             ["icon":"img_starred_icon","title": ManageLocalization.getLocalaizedString(key: "Starred")],
                                             ["icon":"img_draft_icon","title":ManageLocalization.getLocalaizedString(key: "Draft")],
                                             ["icon":"img_spam_icon","title":ManageLocalization.getLocalaizedString(key: "Spam")],
                                             ["icon":"img_trash_icon","title":ManageLocalization.getLocalaizedString(key: "Trash")],
                                             ["icon":"img_contact_us_icon","title":ManageLocalization.getLocalaizedString(key: "Contact Us")],
                                             ["icon":"img_terms_condition_icon","title":ManageLocalization.getLocalaizedString(key: "Terms and Conditions")]])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.endEditing(true)
        tblMenu.tableFooterView = UIView()
        self.tblMenu.reloadData()

        // Do any additional setup after loading the view.
    }
    
 /*   override func viewWillAppear(_ animated: Bool) {
      
      self.tblMenu.reloadData()
//        imgUserImage.CircularImageWith(color: UIColor.lightGray.cgColor, borderWidth: 2.0)
                if AppTheme.sharedInstance.currentUserData.count == 0 {
                    AppTheme.sharedInstance.getUserDataFromDefault()
                }
        if AppTheme.sharedInstance.currentUserData.count > 0 {
            print(String(format : "%@%@",APPURL.BaseUrl, AppTheme.sharedInstance.currentUserData[0].UserImage))
            lblName.text = AppTheme.sharedInstance.currentUserData[0].Name
            imgUserImage.sd_setImage(with: URL.init(string: String(format : "%@%@",APPURL.BaseUrl, AppTheme.sharedInstance.currentUserData[0].UserImage)), placeholderImage: #imageLiteral(resourceName: "icon.UserImage"), options: SDWebImageOptions.highPriority, completed: nil)
        }
        
        //self.view.semanticContentAttribute = .forceRightToLeft
        // self.viewMenu.semanticContentAttribute = .forceRightToLeft
        //        if ManageLocalization.deviceLang == "ar" {
        //          self.frostedViewController.direction = .right
        //             UIView.appearance().semanticContentAttribute = .forceRightToLeft
        //        }
        //        }else{
        //          self.frostedViewController.direction = .left
        //            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        //        }
        
        
        //        self.view.textAlignment = .right

  }*/
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func function_SelectMenu(identifier:String) {
        let navigationController = self.storyboard?.instantiateViewController(withIdentifier: "DrawerRootNavigationController") as! UINavigationController
        navigationController.viewControllers = [self.storyboard?.instantiateViewController(withIdentifier: identifier) as! SplashViewController]
        self.frostedViewController.contentViewController = navigationController
        self.frostedViewController.hideMenuViewController()
        //        if ManageLocalization.deviceLang == "ar" {
        //            self.frostedViewController.direction = .left
        //        }else{
        //            self.frostedViewController.direction = .right
        //        }
    }
    
    @IBAction func btnMenuItems(sender : UIButton){
        let navigationController = self.storyboard?.instantiateViewController(withIdentifier: "DrawerRootNavigationController") as! UINavigationController
        self.frostedViewController.contentViewController = navigationController
        self.frostedViewController.hideMenuViewController()
    }
    
    @IBAction func btnCloseDrawer(sender : UIButton){
        self.frostedViewController.hideMenuViewController()
    }
    
    @IBAction func btnLogout(sender : UIButton){
    }
}

extension MenuViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : cellMenu = tableView.dequeueReusableCell(withIdentifier: "cellMenu") as! cellMenu
        let dict = RV_CheckDataType.getDictionary(anyDict: arrMenuList.object(at: indexPath.row) as AnyObject)
        cell.lblTitle.localizedText = RV_CheckDataType.getString(anyString: dict.value(forKey: "title") as AnyObject)
        let imageName = RV_CheckDataType.getString(anyString: dict.value(forKey: "icon") as AnyObject)
        cell.imgIcon.image = UIImage(named: imageName)
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
//        switch indexPath.row {
//        case 0:
//           let HomeTabBar : LYTabBar = storyBoard.instantiateViewController(withIdentifier: "LYTabBar") as! LYTabBar
//            let nc = UINavigationController.init(rootViewController: HomeTabBar)
//            nc.setNavigationBarHidden(true, animated: false)
//            self.frostedViewController.contentViewController = nc
//            self.frostedViewController.hideMenuViewController()
//
//            break
//        case 1:
//           let FollowingVC : FollowingViewController = SecondStoryBoard.instantiateViewController(withIdentifier: "FollowingViewController") as! FollowingViewController
//            let nc = UINavigationController.init(rootViewController: FollowingVC)
//            nc.setNavigationBarHidden(false, animated: false)
//            self.frostedViewController.contentViewController = nc
//            self.frostedViewController.hideMenuViewController()
//
//            break
//        case 2:
//           let FollowersVC : FollowersViewController = SecondStoryBoard.instantiateViewController(withIdentifier: "FollowersViewController") as! FollowersViewController
//            let nc = UINavigationController.init(rootViewController: FollowersVC)
//            nc.setNavigationBarHidden(false, animated: false)
//            self.frostedViewController.contentViewController = nc
//            self.frostedViewController.hideMenuViewController()
//            break
//        case 3:
//            let ViewsVC : ViewsViewController = SecondStoryBoard.instantiateViewController(withIdentifier: "ViewsViewController") as! ViewsViewController
//            let nc = UINavigationController.init(rootViewController: ViewsVC)
//            nc.setNavigationBarHidden(false, animated: false)
//            self.frostedViewController.contentViewController = nc
//            self.frostedViewController.hideMenuViewController()
//
//            break
//        case 4:
//          let SettingsVC : SettingsViewController = SecondStoryBoard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
//            let nc = UINavigationController.init(rootViewController: SettingsVC)
//            nc.setNavigationBarHidden(false, animated: false)
//            self.frostedViewController.contentViewController = nc
//            self.frostedViewController.hideMenuViewController()
//
//            break
//        case 5:
//           let TicketsVC : TicketsViewController = SecondStoryBoard.instantiateViewController(withIdentifier: "TicketsViewController") as! TicketsViewController
//            let nc = UINavigationController.init(rootViewController: TicketsVC)
//            nc.setNavigationBarHidden(false, animated: false)
//            self.frostedViewController.contentViewController = nc
//            self.frostedViewController.hideMenuViewController()
//
//            break
//        case 6:
//            DispatchQueue.main.async {
//                   Manager.sharedInstance.showAlertWithSignOutAction(self, isAuthFailed: false)
//            }
//        default:
//            break
//        }
    }
}
