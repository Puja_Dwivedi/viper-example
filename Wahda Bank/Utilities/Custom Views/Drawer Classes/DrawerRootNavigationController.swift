//
//  DrawerRootNavigationController.swift
//  SlideMenu
//
//  Created by cis on 17/08/17.
//  Copyright © 2017 cis. All rights reserved.
//

import UIKit

class DrawerRootNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.addGestureRecognizer(UIPanGestureRecognizer.init(target: self, action: #selector(self.panGestureRecognized)))
    }
    @objc func panGestureRecognized(sender:UIPanGestureRecognizer) {
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.panGestureRecognized(sender)
    }
}
