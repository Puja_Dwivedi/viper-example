//
//  UILabel+Extension.swift
//  Wahda Bank
//
//  Created by Apple on 28/01/22.
//

import UIKit

extension UILabel {
    
    @IBInspectable var localizedText: String {
        get {
            return ""
        }
        set {
            self.text = ManageLocalization.getLocalaizedString(key: newValue)
        }
    }
    @IBInspectable override var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        
    }
    @IBInspectable override var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var isUnderLine : Bool {
        get {
            return false
        }
        set {
            let attributedString = NSAttributedString(string: self.text!)
            let textRange = NSMakeRange(0, attributedString.length)
            let underlinedMessage = NSMutableAttributedString(attributedString: attributedString)
            underlinedMessage.addAttribute(NSAttributedString.Key.underlineStyle,
                                           value:NSUnderlineStyle.single.rawValue,
                                           range: textRange)
            self.attributedText = underlinedMessage
        }
    }
    @IBInspectable var showTitleShadow : Bool {
        get {
            return false
        }set{
            self.layer.shadowColor = UIColor.init(hex: "AD5081").cgColor
            self.layer.shadowOffset = CGSize(width: -2, height: 0)
            self.layer.shadowRadius = 5
            self.layer.shadowOpacity = 1.0
        }
    }
    func getAttibutedStringWithTitle(txtTitle : String, txtText : String){
        let lblAttributedString = NSMutableAttributedString(string: "")
        var titleAttribute = [NSAttributedString.Key.font : UIFont.init(name: "Muli-Bold", size: 15.0)!, NSAttributedString.Key.foregroundColor : UIColor.black
            ] as [NSAttributedString.Key : Any]
        let strTitleAttribute = NSMutableAttributedString(string: txtTitle.localized, attributes:titleAttribute)
        titleAttribute = [
            NSAttributedString.Key.font : UIFont.init(name: "Muli", size: 15.0)!,
            NSAttributedString.Key.foregroundColor : UIColor.gray] as [NSAttributedString.Key : Any]
        let textAttribute = NSMutableAttributedString(string: " \(txtText)", attributes:titleAttribute)
        
        lblAttributedString.setAttributedString(strTitleAttribute)
        lblAttributedString.append(textAttribute)
        self.attributedText = lblAttributedString
    }
 }
