//
//  String+Extension.swift
//  Basic Project Architecture
//
//  Created by Apple on 04/01/22.
//

import UIKit

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    var isReallyEmpty: Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    var localized : String {
        return ManageLocalization.getLocalaizedString(key: self)
    }
}
