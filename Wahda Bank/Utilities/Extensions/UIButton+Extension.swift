//
//  UIButton+Extension.swift
//  Wahda Bank
//
//  Created by Apple on 27/01/22.
//

import UIKit

extension UIButton {
    @IBInspectable var localizedTitle: String {
        get {
            return ""
        }
        set {
            self.setTitle(ManageLocalization.getLocalaizedString(key: newValue), for: UIControl.State.normal)
        }
    }
    
    func alignVertical(spacing: CGFloat = 6.0) {
        guard let imageSize = self.imageView?.image?.size,
            let text = self.titleLabel?.text,
            let font = self.titleLabel?.font
            else { return }
        self.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -imageSize.width, bottom: -(imageSize.height + spacing), right: 0.0)
        let labelString = NSString(string: text)
        let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: font])
        self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0.0, bottom: 0.0, right: -titleSize.width)
        let edgeOffset = abs(titleSize.height - imageSize.height) / 2.0;
        self.contentEdgeInsets = UIEdgeInsets(top: edgeOffset, left: 0.0, bottom: edgeOffset, right: 0.0)
    }
    
    @IBInspectable var isUnderLine : Bool {
        get {
            return false
        }
        set {
            let attributedString = NSAttributedString(string: self.title(for: .normal)!)
            let textRange = NSMakeRange(0, attributedString.length)
            let underlinedMessage = NSMutableAttributedString(attributedString: attributedString)
            underlinedMessage.addAttribute(NSAttributedString.Key.underlineStyle,
                                           value:NSUnderlineStyle.single.rawValue,
                                           range: textRange)
            self.setAttributedTitle(underlinedMessage, for: .normal)
        }
    }
    
}
