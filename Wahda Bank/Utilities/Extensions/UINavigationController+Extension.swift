//
//  UINavigationController+Extension.swift
//  Wahda Bank
//
//  Created by Apple on 27/01/22.
//

import UIKit

extension UINavigationController {
  
    func pushViewAnimationFromBottom(){
        let navigationController = self
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: .linear)
        transition.type = .moveIn
        transition.subtype = .fromBottom
        navigationController.view.layer.add(transition, forKey: nil)
    }
    func popviewAnimation(){
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: .easeOut)
        transition.type = .reveal
        transition.subtype = .fromTop
        self.view.layer.add(transition, forKey: nil)
        _ = self.popViewController(animated: false)
        
    }
}
