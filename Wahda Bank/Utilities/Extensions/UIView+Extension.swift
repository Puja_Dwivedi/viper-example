//
//  UIView+Extension.swift
//  Basic Project Architecture
//
//  Created by Apple on 04/01/22.
//

import UIKit

extension UIView{
    @IBInspectable var cornerRadius: CGFloat {
        
        get{
            return layer.cornerRadius
        }
        set {
            DispatchQueue.main.async {
                self.layoutIfNeeded()
                self.layer.cornerRadius = newValue
                self.layer.masksToBounds = newValue > 0
            }
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            DispatchQueue.main.async {
                self.layoutIfNeeded()
                self.layer.borderWidth = newValue
            }
        }
    }
    
    
    @IBInspectable var borderColor: UIColor {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            DispatchQueue.main.async {
                self.layoutIfNeeded()
                self.layer.borderColor = newValue.cgColor
            }
        }
    }
    
    func addShadowTo(radious : CGFloat) {
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: UIRectCorner.bottomLeft.union(.bottomRight), cornerRadii: CGSize(width: 6, height: 6)).cgPath
        self.layer.shadowPath = maskPath
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 2
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath
        self.layer.mask = maskLayer
    }
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    func setNoDataView(title: String, message: String, topBottomMargin : CGFloat) {
        let emptyView = UIView(frame: CGRect(x: 0, y: topBottomMargin/2, width: self.bounds.size.width, height: self.bounds.size.height - 2*topBottomMargin))
        emptyView.tag = 2002
        let titleLabel = UILabel.init(frame: CGRect(x: 5, y: (self.bounds.size.height - 2*topBottomMargin)/2-20, width: self.bounds.size.width-10, height: 40))
        let messageLabel = UILabel.init(frame: CGRect(x: 5, y: titleLabel.frame.maxY + 10, width: self.bounds.size.width-10, height: 60))
        titleLabel.text = title
        messageLabel.text = message
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont(name: "Poppins-Bold", size: 17)
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont(name: "Poppins-Regular", size: 14)
        emptyView.addSubview(titleLabel)
        emptyView.addSubview(messageLabel)
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        // The only tricky part is here:
        emptyView.center = self.center
        self.addSubview(emptyView)
    }
    func restoreView() {
        for view in self.subviews {
            if view.tag == 2002{
                view.removeFromSuperview()
                return
            }
        }
    }
}
@IBDesignable
public class Gradient: UIView {
    @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
    
    override public class var layerClass: AnyClass { CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { layer as! CAGradientLayer }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? .init(x: 1, y: 0) : .init(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 0, y: 1) : .init(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? .init(x: 0, y: 0) : .init(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 1, y: 1) : .init(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
        
    }
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updatePoints()
        updateLocations()
        updateColors()
    }
}
