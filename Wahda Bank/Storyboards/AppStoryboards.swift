//
//  AuxStoryboard.swift
//  AUX
//
//  Created by cis on 21/07/20.
//  Copyright © 2020 cis. All rights reserved.
//

import Foundation
import AVKit

class AppStoryboards{
    class func Main(controller: String) -> UIViewController {
        return UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "\(controller)")
    }
}
