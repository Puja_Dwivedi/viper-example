//
//  AppControllers.swift
//  Wahda Bank
//
//  Created by Apple on 25/01/22.
//

import Foundation
import AVKit

class AppControllers{
    
    static var SplashViewController = "SplashViewController"
    
    static var WelcomeViewController = "WelcomeViewController"
    
    static var LoginViewController = "LoginViewController"
    
    static var VerificationCodeViewController = "VerificationCodeViewController"
    
    static var MenuViewController = "MenuViewController"
}
