//
//  VerificationCodeRouter.swift
//  Wahda Bank
//
//  Created by Apple on 28/01/22.
//

import UIKit

class VerificationCodeRouter: VerificationCodePresenterToRouterProtocols {

   static func function_Create_Verification_Code_Module() -> UIViewController{
        let verificationCodeVC =  AppStoryboards.Main(controller: AppControllers.VerificationCodeViewController) as! VerificationCodeViewController
        
        let presenter: VerificationCodeViewToPresenterProtocol & VerificationCodeInteractorToPresenterProtocols = VerificationCodePresenter()
        let interactor:  VerificationCodePresenterToInteractorProtocols = VerificationCodeInteractor()
        let router: VerificationCodePresenterToRouterProtocols = VerificationCodeRouter()
        
        verificationCodeVC.presenter = presenter
        presenter.view = verificationCodeVC
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return verificationCodeVC
    }
}
