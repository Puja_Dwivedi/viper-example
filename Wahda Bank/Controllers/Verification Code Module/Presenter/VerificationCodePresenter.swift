//
//  PresenterVerificationCode.swift
//  Wahda Bank
//
//  Created by Apple on 28/01/22.
//

import UIKit

class VerificationCodePresenter : VerificationCodeViewToPresenterProtocol {
    var view: VerificationCodePresenterToViewProtocols?
    
    var interactor: VerificationCodePresenterToInteractorProtocols?
    
    var router: VerificationCodePresenterToRouterProtocols?
    
    func function_Call_Login_Service() {
        
    }
}

extension VerificationCodePresenter : VerificationCodeInteractorToPresenterProtocols{
    
}
