//
//  VerificationCodeProtocols.swift
//  Wahda Bank
//
//  Created by Apple on 28/01/22.
//

import UIKit 

protocol VerificationCodeViewToPresenterProtocol : AnyObject {
    var view: VerificationCodePresenterToViewProtocols? {get set}
    var interactor: VerificationCodePresenterToInteractorProtocols? {get set}
    var router: VerificationCodePresenterToRouterProtocols? {get set}
    
    func function_Call_Login_Service()
}

protocol VerificationCodePresenterToViewProtocols : AnyObject {
    
}

protocol VerificationCodePresenterToInteractorProtocols : AnyObject {
    var presenter: VerificationCodeInteractorToPresenterProtocols? {get set}
}

protocol VerificationCodeInteractorToPresenterProtocols : AnyObject {
    
}

protocol VerificationCodePresenterToRouterProtocols : AnyObject {
    static func function_Create_Verification_Code_Module() -> UIViewController
}
