//
//  WelcomePresenter.swift
//  Wahda Bank
//
//  Created by Apple on 24/01/22.
//

import UIKit

class SplashPresenter: SplashViewToPresenterProtocol {
    var view: SplashPresenterToViewProtocol?
    var interactor: SplashPresenterToInteractorProtocol?
    var router: SplashPresenterToRouterProtocol?
    
    func function_Check_Login_Status() {
        interactor?.function_Get_LoggedIn_User_Details()
    }
     
    func function_Push_To_Home_Screen(navigationConroller:UINavigationController) {
        router?.function_Push_To_Home_Screen(navigationConroller: navigationConroller)
    }
    
    func function_Push_To_Welcome_Screen(navigationConroller:UINavigationController) {
        router?.pushToWelcomeScreen(navigationConroller: navigationConroller)
    }
    
}

extension SplashPresenter : SplashInteractorToPresenterProtocol {
    func FetchFailedwith(error: String) {
        view?.UserDetailsError(strError: error)
    }
    
    func gotUserDetails() {
        view?.UserDetailsSuccess()
    }
    
   
}
