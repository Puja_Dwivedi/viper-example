//
//  WelcomeInteractor.swift
//  Wahda Bank
//
//  Created by Apple on 24/01/22.
//

import UIKit

class SplashInteractor: SplashPresenterToInteractorProtocol {
    var presenter: SplashInteractorToPresenterProtocol?
    
    func function_Get_LoggedIn_User_Details() {
        presenter?.FetchFailedwith(error: "error")
    }
}
