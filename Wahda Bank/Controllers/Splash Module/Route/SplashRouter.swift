//
//  SplashRouter.swift
//  Wahda Bank
//
//  Created by Apple on 24/01/22.
//

import UIKit


class SplashRouter: SplashPresenterToRouterProtocol{
    
    static func Function_create_Splash_Module() -> UIViewController {
        let navigationController = AppStoryboards.Main(controller: "DrawerRootNavigationController")
        if let SplashVC = navigationController.children.first as? SplashViewController{
            let presenter: SplashViewToPresenterProtocol & SplashInteractorToPresenterProtocol = SplashPresenter()
            let interactor: SplashPresenterToInteractorProtocol = SplashInteractor()
            let router: SplashPresenterToRouterProtocol = SplashRouter()
            
            SplashVC.presentor = presenter
            presenter.view = SplashVC
            presenter.router = router
            presenter.interactor = interactor
            interactor.presenter = presenter
            
            return SplashVC
        }
        return UIViewController()
    }
    
    
    func pushToWelcomeScreen(navigationConroller: UINavigationController) {
        let loginviewcontroller = WelcomeRouter.Function_create_Welcome_Module()
        navigationConroller.popviewAnimation()
        navigationConroller.pushViewController(loginviewcontroller, animated: false)
        
    }
    
    func function_Push_To_Home_Screen(navigationConroller: UINavigationController) {
        
    }
}
