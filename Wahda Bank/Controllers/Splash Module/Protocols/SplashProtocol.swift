//
//  WelcomeProtocol.swift
//  Wahda Bank
//
//  Created by Apple on 24/01/22.
//

import UIKit

protocol SplashViewToPresenterProtocol: AnyObject{
    
    var view: SplashPresenterToViewProtocol? {get set}
    var interactor: SplashPresenterToInteractorProtocol? {get set}
    var router: SplashPresenterToRouterProtocol? {get set}
    
    func function_Check_Login_Status()
    func function_Push_To_Welcome_Screen(navigationConroller:UINavigationController)
    func function_Push_To_Home_Screen(navigationConroller:UINavigationController)
}

protocol SplashPresenterToViewProtocol: AnyObject{
    func showNetworkError()
    func UserDetailsError(strError : String)
    func UserDetailsSuccess()
}

protocol SplashPresenterToRouterProtocol: AnyObject {
    static func Function_create_Splash_Module()-> UIViewController
    func pushToWelcomeScreen(navigationConroller:UINavigationController)
    func function_Push_To_Home_Screen(navigationConroller:UINavigationController)
}

protocol SplashPresenterToInteractorProtocol: AnyObject {
    var presenter: SplashInteractorToPresenterProtocol? {get set}
    func function_Get_LoggedIn_User_Details()
}

protocol SplashInteractorToPresenterProtocol: AnyObject {
    func FetchFailedwith(error : String)
    func gotUserDetails()
    
}
