//
//  SplashViewController.swift
//  Wahda Bank
//
//  Created by Apple on 24/01/22.
//

import UIKit

class SplashViewController: UIViewController {
    
    var presentor : SplashViewToPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        let secondsToDelay = 5.0
        DispatchQueue.main.asyncAfter(deadline: .now() + secondsToDelay) {
            self.presentor?.function_Check_Login_Status()
        }
    }
}

extension SplashViewController : SplashPresenterToViewProtocol{
    func showNetworkError() {
        
    }
    func UserDetailsError(strError : String) {
        presentor?.function_Push_To_Welcome_Screen(navigationConroller: self.navigationController!)
    }
    func UserDetailsSuccess() {
        presentor?.function_Push_To_Home_Screen(navigationConroller: self.navigationController!)
    }
    
}
