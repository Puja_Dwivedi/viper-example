//
//  LoginProtocol.swift
//  Wahda Bank
//
//  Created by Apple on 24/01/22.
//

import UIKit

protocol LoginViewToPresenterProtocol: AnyObject{
    var view: LoginPresenterToViewProtocol? {get set}
    var interactor: LoginPresenterToInteractorProtocol? {get set}
    var router: LoginPresenterToRouterProtocol? {get set}
    
    func function_Perform_Login_Service()
    
    func function_Push_to_Verification_Code(navigationController : UINavigationController)
}

protocol LoginPresenterToViewProtocol: AnyObject{
    func showError()
}

protocol LoginPresenterToInteractorProtocol: AnyObject {
    var presenter: LoginInteractorToPresenterProtocol? {get set}
    func function_Get_User_Details()
}

protocol LoginInteractorToPresenterProtocol: AnyObject {

}

protocol LoginPresenterToRouterProtocol: AnyObject {
    static func function_Create_Login_Module() -> UIViewController
    func function_Push_to_Verification_Code(navigationController : UINavigationController)
}
