//
//  LoginRoute.swift
//  Wahda Bank
//
//  Created by Apple on 25/01/22.
//

import UIKit

class LoginRouter: LoginPresenterToRouterProtocol{
    
    static func function_Create_Login_Module() -> UIViewController {
        
        let loginVC =  AppStoryboards.Main(controller: AppControllers.LoginViewController) as! LoginViewController
        
        let presenter: LoginViewToPresenterProtocol & LoginInteractorToPresenterProtocol = LoginPresenter()
        let interactor:  LoginPresenterToInteractorProtocol = LoginInteractor()
        let router: LoginPresenterToRouterProtocol = LoginRouter()
        
        loginVC.presenter = presenter
        presenter.view = loginVC
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return loginVC
    }
    
    func function_Push_to_Verification_Code(navigationController: UINavigationController) {
        let verificationcode = VerificationCodeRouter.function_Create_Verification_Code_Module()
        navigationController.pushViewController(verificationcode, animated: true)
    }
    
    
}
