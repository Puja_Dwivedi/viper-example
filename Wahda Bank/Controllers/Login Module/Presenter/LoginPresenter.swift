//
//  LoginPresenter.swift
//  Wahda Bank
//
//  Created by Apple on 25/01/22.
//

import UIKit

class LoginPresenter: LoginViewToPresenterProtocol{
  
    var view: LoginPresenterToViewProtocol?
    var interactor: LoginPresenterToInteractorProtocol?
    var router: LoginPresenterToRouterProtocol?
    
    func function_Perform_Login_Service() {
        interactor?.function_Get_User_Details()
    }
    
    func function_Push_to_Verification_Code(navigationController: UINavigationController) {
        router?.function_Push_to_Verification_Code(navigationController: navigationController)
    }
    
}

extension LoginPresenter: LoginInteractorToPresenterProtocol{
    
}
