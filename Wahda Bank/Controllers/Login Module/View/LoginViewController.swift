//
//  WelcomeViewController.swift
//  Wahda Bank
//
//  Created by Apple on 24/01/22.
//

import UIKit

class LoginViewController: UIViewController {
    
    var presenter: LoginViewToPresenterProtocol?
    
//    @IBOutlet var txtFldEmailAddress : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupView()
    }
    func setupView(){
        self.navigationController?.isNavigationBarHidden = true
        self.navigationItem.hidesBackButton = true
    }
    
    @IBAction func btnLoginClicked( _ sender: UIButton){
        presenter?.function_Push_to_Verification_Code(navigationController: self.navigationController!)
    }
}

extension LoginViewController : LoginPresenterToViewProtocol{
    func showError() {
        
    }
    
    
}
