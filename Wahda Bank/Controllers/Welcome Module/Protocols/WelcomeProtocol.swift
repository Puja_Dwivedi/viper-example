//
//  WelcomeProtocol.swift
//  Wahda Bank
//
//  Created by Apple on 24/01/22.
//

import UIKit

protocol WelcomeViewToPresenterProtocol: AnyObject{
    
    var view: WelcomePresenterToViewProtocol? {get set}
    var interactor: WelcomePresenterToInteractorProtocol? {get set}
    var router: WelcomePresenterToRouterProtocol? {get set}
    
    func function_Move_To_Login_Screen(navigationController: UINavigationController)
}

protocol WelcomePresenterToViewProtocol: AnyObject{

}

protocol WelcomePresenterToRouterProtocol: AnyObject {
    static func Function_create_Welcome_Module()-> UIViewController
    func Function_Push_To_Login_Screen(navigationController : UINavigationController)
}

protocol WelcomePresenterToInteractorProtocol: AnyObject {
    var presenter: WelcomeInteractorToPresenterProtocol? {get set}
}

protocol WelcomeInteractorToPresenterProtocol: AnyObject {
   
    
}
