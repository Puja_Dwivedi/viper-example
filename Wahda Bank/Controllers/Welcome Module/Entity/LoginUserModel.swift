//
//  LoginUserModel.swift
//  Wahda Bank
//
//  Created by Apple on 24/01/22.
//

import UIKit

class LoginUserModel: Codable {
    let id: String
    let userFirstName, userLastName, userEmail, userPhoneNumber, userToken: String?
    let userCreateOn, FCMToken: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case userEmail = "email"
        case userCreateOn = "registered_at"
        case userFirstName = "fname"
        case userLastName = "lname"
        case userPhoneNumber = "phone"
        case userToken = "token"
        case FCMToken = "fcm_token"
    }
}

