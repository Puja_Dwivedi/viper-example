//
//  WelcomeRouter.swift
//  Wahda Bank
//
//  Created by Apple on 24/01/22.
//

import UIKit

class WelcomeRouter: WelcomePresenterToRouterProtocol{
    
    static func Function_create_Welcome_Module() -> UIViewController {
        
        let WelcomeVC = AppStoryboards.Main(controller: AppControllers.WelcomeViewController) as! WelcomeViewController
            let presenter: WelcomeViewToPresenterProtocol & WelcomeInteractorToPresenterProtocol = WelcomePresenter()
            let interactor: WelcomePresenterToInteractorProtocol = WelcomeInteractor()
            let router: WelcomePresenterToRouterProtocol = WelcomeRouter()
            
            WelcomeVC.presentor = presenter
            presenter.view = WelcomeVC
            presenter.router = router
            presenter.interactor = interactor
            interactor.presenter = presenter
            
            return WelcomeVC
    }
    
    
    func Function_Push_To_Login_Screen(navigationController : UINavigationController){
        let loginviewcontroller = LoginRouter.function_Create_Login_Module()
        navigationController.pushViewController(loginviewcontroller, animated: true)
        
    }
}
