//
//  WelcomeViewController.swift
//  Wahda Bank
//
//  Created by Apple on 24/01/22.
//

import UIKit

class WelcomeViewController: UIViewController {
    @IBOutlet var btnGetStarted : UIButton!
    var presentor : WelcomeViewToPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
    }
    
    @IBAction func btnGetStartedAction( _ sender : UIButton){
        presentor!.function_Move_To_Login_Screen(navigationController: self.navigationController!)
    }
}

extension WelcomeViewController : WelcomePresenterToViewProtocol{
    
}
