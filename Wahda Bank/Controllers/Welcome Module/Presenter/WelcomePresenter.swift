//
//  WelcomePresenter.swift
//  Wahda Bank
//
//  Created by Apple on 24/01/22.
//

import UIKit

class WelcomePresenter: WelcomeViewToPresenterProtocol {
    var view: WelcomePresenterToViewProtocol?
    var interactor: WelcomePresenterToInteractorProtocol?
    var router: WelcomePresenterToRouterProtocol?
    
    func function_Move_To_Login_Screen(navigationController: UINavigationController) {
        router?.Function_Push_To_Login_Screen(navigationController: navigationController)
    }
}

extension WelcomePresenter : WelcomeInteractorToPresenterProtocol{
    
}
